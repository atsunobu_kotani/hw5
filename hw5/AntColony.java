package hw5;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

public class AntColony {
	private FileReader fr;
	private BufferedReader br;
	private ArrayList<String[]> cities;
    
    // number of ants used = numAntFactor * # cities
	// There are extra ants that follow greedy algorithm.
    private double numAntFactor = 0.8;
    public int n = 0; // # cities
    public int m = 0; // # ants

    // Number of iterations
    private int maxIterations = 2000;

    private double graph[][] = null;
    private double trails[][] = null;
    private Ant ants[] = null;
    private Ant greedyAnts[] = null;
    private Random rand = new Random();
    private double probs[] = null;
    private int currentIndex = 0;
    public int[] bestTour;
    private int[] gTour;
    public double bestTourLength;

    // Ant class
    private class Ant {
        public int tour[] = new int[graph.length];
        // Each individual ant maintains a visited list for cities
        public boolean visited[] = new boolean[graph.length];
        // In order to determine whether an ant visited the specific
        // city or not is to check the boolean visited on that city.
        public void visitCity(int city) {
        	tour[currentIndex + 1] = city;
            visited[city] = true;
        }
        public boolean visited(int i) {
            return visited[i];
        }
        
        // Each ant keeps track of tour length.
        public double tourLength() {
            double length = graph[tour[n - 1]][tour[0]];
            for (int i = 0; i < n - 1; i++) {
                length += graph[tour[i]][tour[i + 1]];
            }
            return length;
        }
        public void clear() {
            for (int i = 0; i < n; i++)
                visited[i] = false;
        }
    }

    // Store in probs array the probability of moving to each city
    // Ants like to follow stronger/shorter trails more
    private void probTo(Ant ant) {
        int i = ant.tour[currentIndex];
        // trail preference
        double alpha = 1;
        // greedy preference
        double beta = 5;
        double denom = 0.0;
        for (int l = 0; l < n; l++){
            if (!ant.visited(l)){
                denom += Math.pow(trails[i][l], alpha) * Math.pow(1.0 / graph[i][l], beta);
            }
        }
        for (int j = 0; j < n; j++) {
            if (ant.visited(j)) {
                probs[j] = 0.0;
            } else {
                double numerator = Math.pow(trails[i][j], alpha) * Math.pow(1.0 / graph[i][j], beta);
                probs[j] = numerator / denom;
            }
        }
    }

    // Given an ant select the next city based on the probabilities of each city
    private int selectNextCity(Ant ant) {
        // sometimes just randomly select the next city
        double randomProb = 0.01;
        if (rand.nextDouble() < randomProb) {
            int t = rand.nextInt(n - currentIndex); // random city
            int j = -1;
            for (int i = 0; i < n; i++) {
                if (!ant.visited(i)){
                    j++;
                }
                if (j == t){
                    return i;
                }
            }
        }
        // calculate probabilities for each city
        probTo(ant);
        // randomly select according to probs
        double r = rand.nextDouble();
        double totalProb = 0;
        for (int i = 0; i < n; i++) {
            totalProb += probs[i];
            if (totalProb >= r){
                return i;
            }
        }
    	double minimum = Double.POSITIVE_INFINITY;
        for(int k=0; k<graph.length; k++){
        	if(graph[ant.tour[currentIndex]][k] < minimum){
        		minimum = graph[ant.tour[currentIndex]][k];
        		return k;
        	}
        }
        throw new RuntimeException("This shouldn't be called");
    }

    // m ants with random start city
    private void setupAnts() {
        currentIndex = -1;
        for (int i = 0; i < m; i++) {
            ants[i].clear();
            ants[i].visitCity(rand.nextInt(n));
        }
        for(int j = 0; j < m/3; j++){
        	greedyAnts[j].clear();
        	greedyAnts[j].visitCity(0);
        }
        currentIndex++;
    }
    
    // Choose the next city for all ants
    private void moveAnts() {
        // each ant follows trails
        while (currentIndex < n - 1) {
            for (Ant a : ants){
                a.visitCity(selectNextCity(a));
            }
            for (Ant ga : greedyAnts){
            	for(int i = 1; i<gTour.length; i++){
                	ga.visitCity(gTour[i]);
            	}
            }
            currentIndex++;
        }
    }
    
    // Greedy algorithm is also used in this algorithm because
    // it can help ants to find better path for most time.
    public int[] firstGreedy(){
    	int currentCity = 0;
    	int nextCity = 0;
    	int tempCity = 0;
    	int counter = -1;
        int[] greedyTour = new int[graph.length];
    	Set<Integer> unvisited = new HashSet<Integer>();
    	for(int number=0; number < graph.length; number++){
    		unvisited.add(number);
    	}
		while(unvisited.size() != 0){
			counter += 1;
            greedyTour[counter] = currentCity;
        	double minimum = Double.POSITIVE_INFINITY;
    		Iterator<Integer> iterator = unvisited.iterator();
    		while(iterator.hasNext()){
				tempCity = iterator.next();
				if(graph[currentCity][tempCity] < minimum){
    				minimum = graph[currentCity][tempCity];
    				nextCity = tempCity;
    			}
    		}
    		unvisited.remove(currentCity);
    		currentCity = nextCity;
    	}
    	return greedyTour;
    }
    
    // Update trails based on ants tours
    private void updateTrails() {
        // evaporation
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                double evaporation = 0.5;
                trails[i][j] *= evaporation;
            }
        }
        // each ants contribution
        for (Ant a : ants) {
            // trail deposit coefficient
            double trailDeposit = 5000;
            double contribution = trailDeposit / a.tourLength();
            for (int i = 0; i < n - 1; i++) {
                trails[a.tour[i]][a.tour[i + 1]] += contribution;
            }
            trails[a.tour[n - 1]][a.tour[0]] += contribution;
        }
    }
    
    private void updateGreedyTrails(){
        for (Ant ga : greedyAnts) {
            // trail deposit coefficient
            double trailDeposit = 5000;
            double contribution = trailDeposit / ga.tourLength();
            for (int i = 0; i < n - 1; i++) {
                trails[ga.tour[i]][ga.tour[i + 1]] += contribution;
            }
            trails[ga.tour[n - 1]][ga.tour[0]] += contribution;
        }
    }

    private void updateBest() {
        if (bestTour == null) {
            bestTour = ants[0].tour;
            bestTourLength = ants[0].tourLength();
        }
        for (Ant a : ants) {
            if (a.tourLength() < bestTourLength) {
                bestTourLength = a.tourLength();
                bestTour = a.tour.clone();
                checkIntersection(bestTour);
            }
        }
    }

    // This checkIntersection method is responsible for finding the path
    // intersecting to itself. In order to do so, I describe the path in terms
    // of Line2D and use intersectsLine() method to check for intersections.
    public void checkIntersection(int[] bestTour){
    	ArrayList<Line2D> lineList = new ArrayList<Line2D>();
    	Hashtable<Point2D, Integer> hash = new Hashtable<Point2D, Integer>();
    	for(int i=0; i<bestTour.length-1; i++){
    		String[] city1 = cities.get(bestTour[i]);
			String[] city2 = cities.get(bestTour[i+1]);
			Point2D point1 = new Point2D.Double(Double.parseDouble(city1[0]), Double.parseDouble(city1[1]));
			Point2D point2 = new Point2D.Double(Double.parseDouble(city2[0]), Double.parseDouble(city2[1]));
			Line2D line = new Line2D.Double(point1, point2);
			lineList.add(line);
			hash.put(point1, bestTour[i]);
    	}
    	String[] cityLast = cities.get(bestTour[bestTour.length-1]);
		String[] cityFirst = cities.get(bestTour[0]);
		Point2D pointl = new Point2D.Double(Double.parseDouble(cityLast[0]), Double.parseDouble(cityLast[1]));
		Point2D pointf = new Point2D.Double(Double.parseDouble(cityFirst[0]), Double.parseDouble(cityFirst[1]));
		Line2D lastLine = new Line2D.Double(pointl, pointf);
    	lineList.add(lastLine);
		hash.put(pointl, bestTour[bestTour.length-1]);
    	
    	for(int j=0; j<lineList.size(); j++){
    		Line2D lineToCheck = lineList.remove(j);
    		double dis1 = Math.sqrt(Math.pow(lineToCheck.getX1() - lineToCheck.getX2(), 2) + Math.pow(lineToCheck.getY1() - lineToCheck.getY2(), 2));
    		
    		for(int k=0; k<lineList.size(); k++){
    			Line2D lineCollider = lineList.get(k);
    			if(lineToCheck.intersectsLine(lineCollider)){
    	    		double dis2 = Math.sqrt(Math.pow(lineCollider.getX1() - lineCollider.getX2(), 2) + Math.pow(lineCollider.getY1() - lineCollider.getY2(), 2));
    				if(dis1 > dis2){
    					Integer badCity1 = hash.get(lineToCheck.getP1());
    					Integer badCity2 = hash.get(lineToCheck.getP2());
    					// Changing the value of X in trails[][] -= X will determine
    					// how much the intersecting line should be depreciated.
    					trails[badCity1][badCity2] -= 20;
    					trails[badCity2][badCity1] -= 20;
    				} else {
    					Integer badCity1 = hash.get(lineCollider.getP1());
    					Integer badCity2 = hash.get(lineCollider.getP2());
    					trails[badCity1][badCity2] -= 20;
    					trails[badCity2][badCity1] -= 20;
    				}
    			}
    		}
    	}
    }

    
    public static void main(String[] args) {
    	AntColony antcol = new AntColony();
    	System.out.println(antcol.solve());	
    }
    
    public int[] solve() {
    	try{
    		cities = new ArrayList<String[]>();
    		fr = new FileReader("input_3.csv");
    		br = new BufferedReader(fr);
    		String line;
    		int lineNumber = 0;
    		int counter = 0;
            
    		while((line = br.readLine()) != null){
    			if(lineNumber != 0){
					String[] str = line.split(",");
		            cities.add(str);
	    			counter += 1;
    			}
    			lineNumber++;
    		}
    		
            graph = new double[counter][counter];

    		for(int i=0; i<counter; i++){
    			for(int j=0; j<counter; j++){
    				if(i != j){
    					String[] city1 = cities.get(i);
    					String[] city2 = cities.get(j);
    					Double city1x = Double.parseDouble(city1[0]);
    					Double city1y = Double.parseDouble(city1[1]);
    					Double city2x = Double.parseDouble(city2[0]);
    					Double city2y = Double.parseDouble(city2[1]);
    					Double distance = Math.sqrt(Math.pow((city1x-city2x),2) + Math.pow((city1y - city2y), 2));    					
    					graph[i][j] = distance;
		            }
    				else{
    					graph[i][j] = Double.POSITIVE_INFINITY;
    				}
    			}
	        }
    
	        n = graph.length;
	        m = (int) (n * numAntFactor);
	        
	        trails = new double[n][n];
	        probs = new double[n];
	        ants = new Ant[m];
	        greedyAnts = new Ant[m/3];
	        for (int j = 0; j < m; j++){
	            ants[j] = new Ant();
    	    }
	        for(int k = 0; k < m/3; k++){
	        	greedyAnts[k] = new Ant();
	        }
    		
	        for (int j = 0; j < n; j++){
    			for (int k = 0; k < n; k++){
                	trails[j][k] = 1.0;
                }
    		}
            int iteration = 0;
            updateGreedyTrails();
            gTour = firstGreedy();
            while (iteration < maxIterations) {
            	updateGreedyTrails();
                setupAnts();
                moveAnts();
                updateTrails();
                updateBest();
                iteration++;
            }
            System.out.println("Best tour length: " + (bestTourLength));
            for(int c = 0; c<bestTour.length; c++){
            	System.out.println(bestTour[c]);
            }
            return bestTour.clone();
    	} catch(IOException e){
    		System.out.println("error");
    	} finally {
    		try{
    			br.close();
    			fr.close();
    		} catch(IOException e) {
    			System.out.println("error2");
    		}
    	}
		return null;
    }
}