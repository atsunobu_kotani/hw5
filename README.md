# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository "tries" to solve TSP(traveling salesman problem).
While there are many algorithms which solves the TSP, I decided to implement ant colony optimization algorithm. (See:http://en.wikipedia.org/wiki/Ant_colony_optimization_algorithms)
This algorithm has several investigators called "ants," and while the program is running, these ants move between cities. Based on the total path length, the path an ant took is evaluated. If the final path is very short, the edges inside that path will be evaluated positively, and if not, they will be evaluated poorly.

It seems that ant colony optimization algorithm is not suitable for larger problems because in order to have a better solution, I need to make more ants run in the graph.

I added a method called firstGreedy and checkIntersection to this algorithm.
firstGreedy is namely a greedy algorithm discussed in the class. I decided to have this method because Ant Colony sometimes get a worse result than the one of greedy algorithm when the number of cities is relatively large.
checkIntersection method checks intersections of the path of an ant. Graphically speaking, when there are intersections, there is always a better path to take. So, I decided to depreciate the longer edge of two intersecting edges so that the algorithm will avoid using that edge later.

At last, if I run this algorithm in a better environment than Macbook pro, I am sure I will be able to make more ants run and have a better result. Hoping that the performance of Mac will dramatically increase in several years, I would like to close this comment. Thank you.